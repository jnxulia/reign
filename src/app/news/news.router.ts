import { Route } from '@angular/router';


import { ListTechnologiesComponent }  from './list-technologies/list-technologies.component';
import {NewsComponent} from './news.component'
export const NewsRoutes: Route[] = [
  	{
        path : '' , 
        component: NewsComponent,
    	children: [
            {
                path: '',
                component: ListTechnologiesComponent
            }
    	]
  	}
];