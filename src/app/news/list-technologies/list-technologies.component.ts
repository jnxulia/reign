import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { DataSource } from '@angular/cdk/collections';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'list-technologies',
  templateUrl: './list-technologies.component.html',
  styleUrls: ['./list-technologies.component.css']
})
export class ListTechnologiesComponent implements OnInit {

  technologies: any;
  displayedColumns = ['title', 'created_at'];
  dataSource = new TechnologiesDataSource(this.api);

  constructor(private api: ApiService  ) { }

  ngOnInit() {
    this.api.getTechnologies()
      .subscribe(res => {
        console.log(res);
        this.technologies = res;
      }, err => {
        console.log(err);
      });
  }
  goto(url : string) : void {
    console.log(url)
    window.open(url, '_blank');
  }
}

export class TechnologiesDataSource extends DataSource<any> {
  constructor(private api: ApiService) {
    super()
  }

  connect() {
    return this.api.getTechnologies();
  }

  disconnect() {

  }
}
