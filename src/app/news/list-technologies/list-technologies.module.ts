import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTechnologiesComponent } from './list-technologies.component';



@NgModule({
    imports: [CommonModule],
    declarations: [ListTechnologiesComponent],
    exports: [ListTechnologiesComponent]
})

export class ListTechnologiesModule { }