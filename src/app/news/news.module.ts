import { NgModule   } from '@angular/core';
import {CommonModule} from '@angular/common';

import { RouterModule ,Routes } from '@angular/router';


import { NewsComponent } from './news.component';

import { HeaderComponent} from '../shared/index';
import { ListTechnologiesComponent } from './list-technologies/list-technologies.component';
import { NewsRoutes} from './news.router'
export const routes: Routes = [
    ...NewsRoutes, 
  { path: '**', component: NewsComponent }
  ];

  import {
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule } from "@angular/material";
@NgModule({
    imports: [
        CommonModule,
        RouterModule ,
        
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule
      
        
    ],
    declarations: [
        NewsComponent, 
        HeaderComponent,ListTechnologiesComponent
        

    ],
    exports: [
        NewsComponent, 
        HeaderComponent 
    ]
})

export class NewsModule { }