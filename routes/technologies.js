var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Techologies = require('../models/Technologies.js');
var https = require('https');
/* GET ALL TechologiesS */
router.get('/', function(req, res, next) {
  Techologies.find({} , function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE Techologies BY ID */
router.get('/:id', function(req, res, next) {
  Techologies.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE Techologies */
router.post('/', function(req, res, next) {
  var options = {
    protocol:'https:',  
    host: 'hn.algolia.com',
    port : 443 ,
    path : '/api/v1/search_by_date?query=nodejs',
  };

  var httpreq = https.request(options, function (response) {
    var str = '';

    //another chunk of data has been recieved, so append it to `str`
    response.on('data', function (chunk) {
      str += chunk;
      
    });
  
    //the whole response has been recieved, so we just print it out here
    response.on('end', function () {
      var jsonTechnologies = JSON.parse(str).hits;
      for (let i = 0; i < jsonTechnologies.length; i++) {
        const technology = jsonTechnologies[i];
        if ((technology.title == null) && (technology.story_title == null )) continue; 
        Techologies.update({objectID:  technology.objectID}, technology, {upsert: true, setDefaultsOnInsert: true},  
          function(error, result) {
          if (error) res.json(error);
            if (i  + 1 ==jsonTechnologies.length){
              res.json(jsonTechnologies)
            }
          // do something with the document
      });
      }

      
    });
  });
  httpreq.end();
  //res.json({x : 1});
});

/* UPDATE Techologies */
router.put('/:id', function(req, res, next) {
  Techologies.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE Techologies */
router.delete('/:id', function(req, res, next) {
  Techologies.findByIdAndUpdate(req.params.id, {deleted : true} ,  function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
