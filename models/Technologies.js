var mongoose = require('mongoose');

var TechnologiesSchemas = new mongoose.Schema({
  objectID : Number, 
  story_title: String,
  title: String,
  author: String,
  story_url: String,
  url: String,
  created_at : { type: Date, default: Date.now },
  deleted : Boolean
});

module.exports = mongoose.model('Technologies', TechnologiesSchemas);
